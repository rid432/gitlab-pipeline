class Calculator
  class << self
    def sum(value1, value2)
      value1 + value2
    end

    def multiply(value1, value2)
      value1 * value2
    end

    def average(values)
      return if values.empty?

      sum = values.inject(0, :+)
      sum / values.size.to_f
    end
  end
end
